var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const validInfo = require("../middleware/validInfo");
const jwtGenerator = require("../utils/jwtGenerator");
const authorize = require("../middleware/authorize");
const Pool = require("pg").Pool;
const pool1 = new Pool({
    host: "localhost",
    user: "postgres",
    password: "Anu@1996",
    port: 5432,
    database: "assignment"
});
router.post("/register", validInfo, (req, res) => __awaiter(this, void 0, void 0, function* () {
    const { email, name, password } = req.body;
    try {
        const user = yield pool1.query("SELECT * FROM users WHERE user_email = $1", [
            email
        ]);
        if (user.rows.length > 0) {
            return res.status(401).json("User already exist!");
        }
        const salt = yield bcrypt.genSalt(10);
        const bcryptPassword = yield bcrypt.hash(password, salt);
        let newUser = yield pool1.query("INSERT INTO users (user_name, user_email, user_password) VALUES ($1, $2, $3) RETURNING *", [name, email, bcryptPassword]);
        const jwtToken = jwtGenerator(newUser.rows[0].user_id);
        return res.json({ jwtToken });
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
}));
router.post("/login", validInfo, (req, res) => __awaiter(this, void 0, void 0, function* () {
    const { email, password } = req.body;
    try {
        const user = yield pool1.query("SELECT * FROM users WHERE user_email = $1", [
            email
        ]);
        if (user.rows.length === 0) {
            return res.status(401).json("Invalid Credential");
        }
        const validPassword = yield bcrypt.compare(password, user.rows[0].user_password);
        if (!validPassword) {
            return res.status(401).json("Invalid Credential");
        }
        const jwtToken = jwtGenerator(user.rows[0].user_id);
        return res.json({ jwtToken });
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
}));
router.post("/verify", authorize, (req, res) => {
    try {
        res.json(true);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});
router.route('/uuuu').get((req, res) => {
    console.log('In clients');
    pool1.query('select * from users', (err, res) => {
        console.log(res);
        if (err) {
            console.log(err, res);
            return;
        }
        // client.end();
    });
});
module.exports = router;
//# sourceMappingURL=jwtAuth.js.map