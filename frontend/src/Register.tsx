import React, { useState } from 'react';
import { Form, Input, Button, Checkbox, Select, notification } from 'antd';
import 'antd/dist/antd.css';
import './Register.css'
import axios from "axios";
import { useHistory } from "react-router";

const { Option } = Select;
const API_URL = "http://localhost:3001/users/";


function Register(props:any) {

    function register(name: String, email: String, password: String) {
        return axios.post(API_URL + "signup", {
            name,
            email,
            password,
        });
        // return axios.get(API_URL+"users");
    };
    let history = useHistory();


    const [loading, setLoading] = useState(false);


    const [values, setValues] = useState({
        name: "",
        email: "",
        // age: "",
        // gender: "",
        // address: "",
        // dob: "",
        password: "",
    })
    const [successful, setSuccessful] = useState(false);
    const [message, setMessage] = useState("hj");

    const handleRegister = (e: any) => {
        e.preventDefault();


        const res = register(values.name, values.email, values.password).then(
            (response) => {
                notification.open({
                    message: 'SignIn to continue',

                });
                const jwt = response.data.jwtToken;
                localStorage.setItem("token", jwt);


                history.push("/login");

                console.log(jwt);
            },
            (error) => {
                console.log(error.toString());

            }
        );
        console.log("Here::" + localStorage.getItem("token"));
    };

    function submit() {
        console.log("Submit clicked");
    }

    function handleChange(value: string) {
        console.log(`selected ${value}`);
        // setValues({...values, gender: value });
    }
    const handleNameInputChange = (event: any) => {
        console.log(event.target.value);
        setValues({ ...values, name: event.target.value });
        console.log("Name" + values.name);
    }

    const handleEmailInputChange = (event: any) => {
        setValues({ ...values, email: event.target.value });
    }


    const handlePasswordInputChange = (event: any) => {
        setValues({ ...values, password: event.target.value });

    }

    const handleSubmitted = (event: any) => {
        event.preventDefault();

    }



    return (
        <div className="centered">
            <Form>

                <h2>Register</h2>

                <Form.Item

                    name="name"
                    rules={[{ required: true, message: 'Please input Your name!' }]}
                >
                    <Input placeholder="Name" value={values.name} onChange={handleNameInputChange} />
                </Form.Item>

                <Form.Item

                    name="email"
                    rules={[{ type: "email", required: true, message: 'Please input valid Email!' }]}
                >
                    <Input placeholder="Email" value={values.email} onChange={handleEmailInputChange} />
                </Form.Item>


                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password placeholder="Password" value={values.password} />
                </Form.Item>


                <Form.Item >
                    <Button type="primary" style={{ background: "orange" }} onClick={handleRegister} block>
                        Register
                </Button>
                </Form.Item>
                <h4>OR</h4>
                <Form.Item >
                    <Button type="primary" style={{ background: "green" }} onClick={()=>props.onSubmit(1)} block>
                        Login
                </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default Register