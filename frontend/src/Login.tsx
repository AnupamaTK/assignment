import React, { useState } from 'react';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import LoginForm from './LoginForm'
import Register from './Register'
import travelImg from './travel.jpg';
import './Login.css'


function Login() {

    const [showLogin, setShowLogin] = useState(false);

    function onSubmit(formState: any) {
        console.log("changing child");
        setShowLogin(!showLogin);
        console.log(showLogin);
    }
    return (


        <div >
            {showLogin ? 
            <div className="rightN"  >
                <Register onSubmit={onSubmit}/>
            </div> : <div className="rightN"  >
                    <LoginForm onSubmit={onSubmit} />
             </div>

            }
            <div className="leftN" id="hideDiv">
                <img src={travelImg} alt="Travel Image" width="100%" height="700hv" />

            </div>


        </div>

    );
}

/*<div className="row" >
          

          <BrowserRouter>
              <div>
                  <Switch>
                      <Route path="/loginform">
                        
                          <h2>h2</h2>
                      </Route>
                      <Route path="/register">
                          <Register />
                      </Route>
                  </Switch>
              </div>
          </BrowserRouter>
      </div>*/

/*let Login = () => {
  return (
    <div className="form-group text-left">
              
                <input type="email" 
                       id="email" 
                       placeholder="Enter email"
                />
              
               
                    <input type="password" 
                        id="password" 
                        placeholder="Password"
                    />
                </div>
   
  );
};*/

export default Login;