import React, { useState, useEffect } from 'react';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import LoginForm from './LoginForm'
import Register from './Register'
import Login from './Login'
import Dashboard from './Dashboard'

function App() {
  const [currentUser, setCurrentUser] = useState(undefined);


  const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem('currentUser') || '{}');
  };

  useEffect(() => {
    // const user = AuthService.getCurrentUser();

    if (getCurrentUser) {
      setCurrentUser(getCurrentUser);
    }
  }, []);


  return (

    <BrowserRouter>
      <div>
        <Switch>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route path="/loginForm">
            <LoginForm />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>


  );
}


export default App;