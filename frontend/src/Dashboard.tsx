import React, { FC, useState, useEffect } from 'react';
import './Dashboard.css';
import ReactDOM from 'react-dom';
import axios from "axios";

import { Form, Input, Button, Checkbox, Table, Typography } from 'antd';
import 'antd/dist/antd.css';

import { Layout, Menu } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    UploadOutlined,
    LogoutOutlined
} from '@ant-design/icons';
import { useHistory } from "react-router";


const API_URL = "http://localhost:3001/users/";
const { Header, Sider, Content } = Layout;
const { Title } = Typography;

function Dashboard() {

    let history = useHistory();

    const [search, setSearch] = useState("");
    const [users, setUsers] = useState([]);
 
    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Age',
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: 'Gender',
            dataIndex: 'gender',
            key: 'gender',
        },
        {
            title: 'DOB',
            dataIndex: 'dob',
            key: 'dob',
        }
    ];


    useEffect(() => {
        axios.get(API_URL + 'users', { headers: authHeader() })
            .then((res: any) => {
                const userList = res.data;
                setUsers(userList);
            })
    }, [])

    function authHeader() {

        const token = JSON.parse(localStorage.getItem('token') || '{}');

        // console.log("TOKEN" + token);

        if (token) {
            return { 'x-access-token': token };
        } else {
            return {};
        }
    }

    const getUsers = () => {
        console.log("In getUsers");
        return axios.get(API_URL + 'users', { headers: authHeader() }).then((response) => {

            const userList = response.data;
            setUsers(userList);

            console.log("hy" + userList[0]);
            // console.log("this inget user before UsersList"+users[0]);



            return response.data;
        });
    }

    const logout = () => {
        window.localStorage.clear();
        history.push("/login");


    }



    const handleUserList = (e: any) => {
        // e.preventDefault();
        console.log("Getting User list");


        getUsers().then(
            () => {
                console.log("Hy");
                console.log(users);
                // window.location.reload();

            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();


            }
        );

    };

    return (
        <Layout>
            <Sider trigger={null} collapsible >

                <div className="logo" />

                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>

                    <Menu.Item key="1" icon={<UserOutlined />}>
                        Users
                     </Menu.Item>
                    <Menu.Item key="1" icon={<LogoutOutlined />} onClick={logout}>
                        Logout
                     </Menu.Item>

                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background" style={{ padding: 0 }}>

                </Header>

                <Content
                    className="site-layout-background"
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                    }}
                >
                    <Title level={5}>Users</Title>

                    <Button style={{ backgroundColor: "#1890ff", color: "white", float: "right" }}>Search</Button>
                    <Input style={{ float: "right", width: "20%" }} value={search} />

                    <Table style={{ marginTop: "2%" }} columns={columns} dataSource={users} />
                </Content>

            </Layout>
        </Layout>
    );
}


export default Dashboard;