import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:8080/api/test/";

const getDashboard = () => {
  return axios.get(API_URL + "all");
};


export default {
  getDashboard,
};