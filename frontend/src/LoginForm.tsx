import React, { useState } from 'react';
import { Form, Input, Button, Checkbox, notification } from 'antd';
import 'antd/dist/antd.css';
import './LoginForm.css'
// import AuthServices from './services/auth.service';
import axios from "axios";
import { useHistory } from "react-router";

const API_URL = "http://localhost:3001/users/";




function Login(props:any) {
// const Login = ({ task, sendId }) => (
    const [values, setValues] = useState({

        email: "",
        password: "",
    })
    const [users, setUsers] = useState([]);
    let history = useHistory();

    


    function authHeader() {

        const token = JSON.parse(localStorage.getItem('token') || '{}');

        console.log("TOKEN" + token);

        if (token) {
            return { 'x-access-token': token };
        } else {
            return {};
        }
    }

    const handlePasswordInputChange = (event: any) => {
        setValues({ ...values, password: event.target.value });
        console.log("PW Changed");
    }

    const handleEmailInputChange = (event: any) => {
        setValues({ ...values, email: event.target.value });
        console.log("email Changed");
    }


    const userLogin = (email: String, password: String) => {
        console.log("Login");

        return axios.post(API_URL + "signin", {
            email,
            password,
        })
            .then((response) => {
                console.log(">>>>>>My Response");
                if (response.data.jwtToken) {
                    localStorage.setItem("token", JSON.stringify(response.data.jwtToken));
                    notification.open({
                        message: 'Logging In',

                    });

                    history.push("/dashboard");
                } else {
                    notification.open({
                        message: 'Error Login',

                    });
                }
                console.log("My response in loging" + response.data.jwtToken);

                return response.data;
            });
    };



    const getUsers = () => {
        console.log("In getUsers");
        return axios.get(API_URL + 'users', { headers: authHeader() }).then((response) => {
            console.log("in RESS" + response.data);
            console.log("RESS 0 data" + response.data[0].email);
            // setUsers([...users, { name: response.data[0].name, email: response.data[0].email}]);
            // setUsers([...users, response.data[0]]);
            // const userList = response.data;
            // console.log(userList[0])
            // setUsers(userList[0]);

            // console.log("hy" + users[0]);

            return response.data;
        });
    }

    const handleUserList = (e: any) => {
        e.preventDefault();
        console.log("Getting User list");

        // setMessage("");
        // setLoading(true);

        //   const history = useHistory();

        getUsers().then(
            () => {
                console.log("Hy");
                console.log(users);
                // window.location.reload();

            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

            }
        );

    };

    const handleLogin = (e: any) => {
        e.preventDefault();
        console.log("USer List");


        //   const history = useHistory();

        userLogin(values.email, values.password).then(
            () => {

                console.log("Gettingggg" + localStorage.getItem('token'))
                // history.push("/profile");
                // window.location.reload();
            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

            }
        );

    };
    return (
        <div className="centered">
            <Form>

                <h2 > Login </h2>


                <Form.Item

                    name="email"
                    rules={[{ type: "email", required: true, message: 'Please input valid Email!' }]} >
                    <Input placeholder="Email" value={values.email} onChange={handleEmailInputChange} />
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]} >
                    <Input.Password placeholder="Password" value={values.password} onChange={handlePasswordInputChange} />
                </Form.Item>

                <Form.Item >
                    <Button type="primary" style={{ background: "green" }} onClick={handleLogin} block>
                        Login
                </Button>
                </Form.Item>
                <h4>Or</h4>

                <Form.Item>
                    <Button type="primary" style={{ background: "orange" }}  onClick={()=>props.onSubmit(1)} block >
                        Register
                </Button>
                </Form.Item >
            </Form> </div>
    )
}

export default Login

